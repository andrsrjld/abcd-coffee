import React, { useState } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar, Badge, Hidden, IconButton } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';
// import { authenticationService } from '../../../../services';
import logo from '../../../../assets/logo/logo-jonas-light.png'

const useStyles = makeStyles(theme => ({
  root: {
    // boxShadow: 'none'
    zIndex: 0
  },
  flexGrow: {
    flexGrow: 1
  },
  signOutButton: {
    marginLeft: theme.spacing(1)
  }
}));

const Topbar = props => {
  // const { history } = props;
  const { className, onSidebarOpen, history, ...rest } = props;

  const classes = useStyles();

  const [notifications] = useState([]);


  const logout = () => {
    // authenticationService.logout();
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    // currentUserSubject.next(null);
    history.push('/sign-in');
  }

  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Toolbar>
        <div className={classes.flexGrow} />
        <Hidden mdDown>
          <IconButton
            className={classes.signOutButton}
            color="inherit"
            title="keluar"
            onClick={logout}
          >
            <InputIcon />
          </IconButton>
        </Hidden>
        <Hidden lgUp>
          <IconButton
            color="inherit"
            onClick={onSidebarOpen}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func,
  history: PropTypes.object
};



export default withRouter(Topbar);

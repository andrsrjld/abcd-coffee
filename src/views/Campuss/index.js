import React, { useEffect, useState } from 'react';
import MyTable from '../../components/MyTablePagination';
import axios from '../../util/Service';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
  Grid,
  Card,
  Drawer,
  Divider,
  Button,
  IconButton,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TableHead,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Snackbar,
  Slide,
  FormControlLabel,
  Switch
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { red, blue } from '@material-ui/core/colors';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import NumberFormatCustom from '../../components/NumberFormatCustom';

const drawerWidth = '80%';
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  },
  content: {
    flexShrink: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    zIndex: '999'
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start'
  },
  buttonAdd: {
    margin: theme.spacing(3)
  }
}));

const fields = ['Nama', 'Alamat', 'Valid Dari', 'Valid Hingga', 'Harga', 'Status'];
const fieldData = ['name', 'address', 'valid_from', 'valid_to', 'price', 'status'];

const Campuss = () => {
  const classes = useStyles();
  const theme = useTheme();

  // Campuss
  const [data, setData] = useState([]);
  const [form, setForm] = useState({
    guid_campuss: null,
    name: '',
    address: '',
    description: '',
    valid_from: new Date(),
    valid_to: new Date(),
    price: 0,
    is_active : true
  });

  // Campuss Detail
  const [openCampussDetail, setOpenCampussDetail] = useState(false);
  const [dataCampussDetail, setDataCampussDetail] = useState([]);
  const [alertDelete, setAlertDelete] = useState(false);
  const [selectedIDCampussDetail, setSelectedIDCampussDetail] = useState(null);
  const [formCampussDetail, setFormCampussDetail] = useState({
    guid_campuss: null,
    schedule: '',
    instructor: "",
    is_active: true
  });

  const [open, setOpen] = useState(false);
  const [page, setPage] = useState(1);
  const [pageCount, setPageCount] = useState(1);
  const [tableLoading, setTableLoading] = useState(false);

  // snackbar
  const [alertt, setAlert] = useState(false);
  const [messageSnackbar, setMessageSnackbar] = useState(null);

  useEffect(() => {
    getCampusses();
  }, [page]);

  // API
  const getCampusses = () => {
    axios({
      method: 'GET',
      url: `v1/campuss?page=${page}&limit=10`,
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => {
        if (response.status === 200 && response.data.data.length != 0) {
          setPageCount(response.data.pagination);
          parsingData(response.data.data);
        }
      })
      .catch(error => {
        alert(error);
      });
  };

  const AddCampuss = () => {
    let dataCampuss = {
      name: form.name,
      address: form.address,
      latitude: "",
      longitude: "",
      description: form.description,
      valid_from: form.valid_from,
      valid_to: form.valid_to,
      price: Number(form.price),
      is_active: form.is_active ? 1 : 0
    };

    axios({
      method: 'POST',
      url: `v1/campuss`,
      headers: { 'Content-Type': 'application/json' },
      data: dataCampuss
    })
      .then((response) => {
        handleShowSnackbar(true, "Berhasil menambahkan campuss");
        
        handleDrawerOpenForEdit(response.data.data.guid_campuss);
      })
      .catch(error => {
        alert(error);
      });
  };

  const UpdateCampuss = () => {
    let dataCampuss = {
      guid_campuss: form.guid_campuss,
      name: form.name,
      address: form.address,
      latitude: "",
      longitude: "",
      description: form.description,
      valid_from: form.valid_from,
      valid_to: form.valid_to,
      price: Number(form.price),
      is_active: form.is_active ? 1 : 0
    };

    axios({
      method: 'PUT',
      url: `v1/campuss`,
      data: dataCampuss,
      headers: { 'Content-Type': 'application/json' }
    })
      .then(() => {
        setPage(1);
        handleDrawerClose();
        handleShowSnackbar(true, "Berhasil update data");
        getCampusses();
      })
      .catch(error => {
        alert(error);
      });
  };

  const deleteCampuss = id => {
    axios({
      method: 'DELETE',
      url: `v1/campuss`,
      headers: { 'Content-Type': 'application/json' },
      params: { guid_campuss: id }
    })
      .then(() => {
        setPage(1);
        getCampusses();
        handleShowSnackbar(true, "Berhasil hapus data");
      })
      .catch(error => {
        alert(error);
      });
  };

  const getCampussDetail = guid_campuss => {
    axios({
      method: 'GET',
      url: `v1/campuss_detail?guid_campuss=` + guid_campuss,
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => {
        if (response.status === 200 && response.data.data != null) {
          console.log(response.data.data)
          setDataCampussDetail(response.data.data);
        } else {
          dataCampussDetail([]);
        }
      })
      .catch(error => {
        alert(error);
      });
  };

  const AddCampussDetail = () => {
    let data_campuss_detail = {
      guid_campuss: form.guid_campuss,
      schedule: formCampussDetail.schedule,
      instructor: formCampussDetail.instructor,
      is_active: formCampussDetail.is_active ? 1 : 0
    };

    axios({
      method: 'POST',
      url: `v1/campuss_detail`,
      headers: { 'Content-Type': 'application/json' },
      data: data_campuss_detail
    })
      .then(() => {
        getCampussDetail(form.guid_campuss);
        handleCampussDetailClose();
        handleShowSnackbar(true, "Berhasil menambahkan campuss detail");
      })
      .catch(error => {
        alert(error);
      });
  };

  const EditCampussDetail = () => {
    let data_campuss_detail = {
      guid_campuss_detail: formCampussDetail.guid_campuss_detail,
      guid_campuss: form.guid_campuss,
      schedule: formCampussDetail.schedule,
      instructor: formCampussDetail.instructor,
      is_active: formCampussDetail.is_active ? 1 : 0
    };

    axios({
      method: 'PUT',
      url: `v1/campuss_detail`,
      data: data_campuss_detail,
      headers: { 'Content-Type': 'application/json' }
    })
      .then(() => {
        getCampussDetail(form.guid_campuss);
        handleCampussDetailClose();
        handleShowSnackbar(true, "Berhasil update data");
      })
      .catch(error => {
        alert(error);
      });
  };

  const DeleteCampussDetail = id => {
    axios({
      method: 'DELETE',
      url: `v1/campuss_detail`,
      headers: { 'Content-Type': 'application/json' },
      params: { guid_campuss_detail: id }
    })
      .then(() => {
        getCampussDetail(form.guid_campuss);
        setAlertDelete(false);
        handleShowSnackbar(true, "Berhasil hapus data");
      })
      .catch(error => {
        alert(error);
      });
  };
  //

  // parsing
  const parsingData = data => {
    if (data != null) {
      const newData = data.map(v => {
        return {
          ...v,
          name: v.name,
          address: v.address,
          description: v.description,
          valid_from: v.valid_from.substring(0,10),
          valid_to: v.valid_to.substring(0,10),
          price: `Rp ${v.price.toLocaleString(navigator.language, {
            minimumFractionDigits: 2
          })}`,
          status: v.is_active === 1 ? "Aktif" : "Tidak Aktif"
        };
      });
      setData(newData);
    }
  };
  //////////

  // handle
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerOpenForEdit = guid_campuss => {
    axios({
      method: 'GET',
      url: `v1/campuss?guid_campuss=` + guid_campuss,
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => {
        const selectCampuss = response.data.data[0];
        
        setForm({
          ...selectCampuss,
          valid_from: selectCampuss.valid_from.substring(0,10),
          valid_to: selectCampuss.valid_to.substring(0,10),
          is_active: 1 ? true : false
        });
        getCampussDetail(selectCampuss.guid_campuss);
        // setDataCampussDetail(selectCampuss.data_campuss_detail);

        handleDrawerOpen();
      })
      .catch(error => {
        alert(error);
      });
  };

  const handleDrawerClose = () => {
    setOpen(false);
    setForm({
      guid_campuss: null,
      name: '',
      address: '',
      description: '',
      valid_from: new Date(),
      valid_to: new Date(),
      price: 0,
      is_active: true
    });
    setDataCampussDetail([]);
  };

  const handleChangeForm = name => e => {

    if(name === 'is_active') {
      setForm({
        ...form,
        [name]: e.target.checked
      });
    } else {
      setForm({
        ...form,
        [name]: e.target.value
      });
    }
  };

  const handleChangeFormCampussDetail = name => e => {

    if(name === 'is_active') {
      setFormCampussDetail({
        ...formCampussDetail,
        [name]: e.target.checked
      });
    } else {
      setFormCampussDetail({
        ...formCampussDetail,
        [name]: e.target.value
      });
    }
  };

  const handleEditCampussDetail = id => {
    const selectCampussDetail = dataCampussDetail.find(v => v.guid_campuss_detail === id);

    setFormCampussDetail({
      ...selectCampussDetail,
      is_active: selectCampussDetail.is_active === 1 ? true : false
    });

    setOpenCampussDetail(true);
  };

  const handleCampussDetailClose = () => {
    setOpenCampussDetail(false);

    setFormCampussDetail({
      guid_campuss: null,
      schedule: '',
      instructor: "",
      is_active: true
    });
  };
  //

  const handleShowSnackbar = (status, message) => {
    setMessageSnackbar(message);
    setAlert(status);
  };

  const handleOnNextPage = () => setPage(page + 1);
  const handleOnBackPage = () => setPage(page - 1);

  return (
    <div className={classes.root}>
      <Grid container spacing={4}>
        <Grid item md={12} xs={12}>
          <Card className={classes.root} variant="outlined">
            <h4>Campuss</h4>
            <hr style={{ marginBottom: '30px' }} />

            <MyTable
              id="guid_campuss"
              data={data}
              fields={fields}
              fieldData={fieldData}
              onAdd={handleDrawerOpen}
              onDelete={deleteCampuss}
              onEdit={handleDrawerOpenForEdit}
              onUpdate={() => {}}
              titleAdd="Tambah Campuss"
              page={page}
              pageCount={pageCount}
              isLoading={tableLoading}
              onNextPageClicked={handleOnNextPage}
              onBackPageClicked={handleOnBackPage}
            />
          </Card>
        </Grid>
      </Grid>

      <Drawer
        className={classes.drawer}
        anchor="right"
        open={open}
        classes={{ paper: classes.drawerPaper }}>
        <div className={classes.drawerHeader}>
          <IconButton
            onClick={() => {
              handleDrawerClose();
            }}>
            {theme.direction === 'rtl' ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
          {form.guid_campuss === null ? 'Simpan' : 'Ubah'} Informasi Campuss
          <Button
            variant="contained"
            color="primary"
            style={{ position: 'absolute', right: '20px', top: '-10px' }}
            className={classes.buttonAdd}
            onClick={
              form.guid_campuss === null ? () => AddCampuss() : () => UpdateCampuss()
            }>
            {form.guid_campuss === null ? 'Simpan' : 'Ubah'}
          </Button>
        </div>
        <Divider />
        <Grid container style={{ padding: theme.spacing(3) }}>
          <Grid item xs={6}>
            <h5>Informasi Campuss</h5>
            <div className="d-flex justify-content-start">
              <FormControlLabel
                control={
                  <Switch
                    checked={form.is_active}
                    onChange={handleChangeForm('is_active')}
                    name="Status_Active"
                    color="primary"
                  />
                }
                label={form.is_active ? "Aktif" : "Tidak Aktif"}
              />
            </div>
            <Grid item xs={10}>
              <TextField
                id="name"
                label="Nama"
                value={form.name}
                onChange={handleChangeForm('name')}
                margin="normal"
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={10}>
              <TextField
                id="address"
                label="Alamat"
                value={form.address}
                onChange={handleChangeForm('address')}
                margin="normal"
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={10}>
              <TextField
                id="description"
                label="Deskripsi"
                value={form.description}
                onChange={handleChangeForm('description')}
                margin="normal"
                fullWidth
                multiline
                rows="3"
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12}>
              <Grid item xs={6} style={{ float: 'left' }}>
                <TextField
                  id="valid_from"
                  label="Valid Dari"
                  value={form.valid_from}
                  onChange={handleChangeForm('valid_from')}
                  type="date"
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Grid>
              <Grid item xs={6} style={{ float: 'left', marginLeft:'2rem' }}>
                <TextField
                  id="valid_to"
                  label="Valid Hingga"
                  value={form.valid_to}
                  onChange={handleChangeForm('valid_to')}
                  type="date"
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Grid>
            </Grid>
            <Grid item xs={6}>
              <TextField
                id="price"
                label="Harga"
                value={form.price}
                onChange={handleChangeForm('price')}
                margin="normal"
                variant="outlined"
                InputProps={{ inputComponent: NumberFormatCustom }}
              />
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <h5>Campuss Detail</h5>
            {form.guid_campuss === null ? (
              <label
                style={{ color: 'red', fontSize: '12px', marginTop: '0' }}>
                *Simpan data campuss terlebih dahulu
              </label>
            ) : (
            <>
              <Grid item xs={12} style={{ marginBottom: '25px' }}>
                {dataCampussDetail !== null ? (
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>Jadwal</TableCell>
                        <TableCell>Instructor</TableCell>
                        <TableCell>Status</TableCell>
                        <TableCell>Action</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {dataCampussDetail && dataCampussDetail.map(v => (
                        <TableRow key={v.guid_campuss_detail}>
                          <TableCell>{v.schedule}</TableCell>
                          <TableCell>{v.instructor}</TableCell>
                          <TableCell>{v.is_active === 1 ? "Aktif" : "Tidak Aktif"}</TableCell>
                          <TableCell>
                            <a
                              onClick={() => {
                                handleEditCampussDetail(v.guid_campuss_detail);
                              }}>
                              <EditIcon
                                style={{ color: blue[500] }}
                                className="pointer mr-2"
                              />
                            </a>
                            <a
                              onClick={() => {
                                setAlertDelete(true);
                                setSelectedIDCampussDetail(v.guid_campuss_detail);
                              }}>
                              <DeleteIcon
                                style={{ color: red[500] }}
                                className="pointer mr-2"
                              />
                            </a>
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                ) : null}
              </Grid>
              <Button
                onClick={() => setOpenCampussDetail(true)}
                variant="contained"
                color="secondary">
                Tambah Campuss Detail
              </Button>
            </>
            )}
          </Grid>
        </Grid>
        <Divider />
      </Drawer>

      {/* DIALOG FOR CampussDetail */}
      <Dialog
        open={openCampussDetail}
        onClose={handleCampussDetailClose}
        aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Campuss Detail</DialogTitle>
        <DialogContent>
          <Grid container style={{ width: '450px' }}>
            <div className="d-flex justify-content-start">
              <FormControlLabel
                control={
                  <Switch
                    checked={formCampussDetail.is_active}
                    onChange={handleChangeFormCampussDetail('is_active')}
                    name="Status_Active"
                    color="primary"
                  />
                }
                label={formCampussDetail.is_active ? "Aktif" : "Tidak Aktif"}
              />
            </div>
            <TextField
              id="schedule"
              label="Jadwal"
              value={formCampussDetail.schedule}
              onChange={handleChangeFormCampussDetail('schedule')}
              margin="normal"
              fullWidth
              variant="outlined"
            />
            <TextField
              id="instructor"
              label="Instructor"
              value={formCampussDetail.instructor}
              onChange={handleChangeFormCampussDetail('instructor')}
              margin="normal"
              fullWidth
              variant="outlined"
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCampussDetailClose} color="primary">
            Tidak
          </Button>
          <Button
            onClick={
              formCampussDetail.guid_campuss_detail === null
                ? () => AddCampussDetail()
                : () => EditCampussDetail()
            }
            variant="contained"
            color="primary">
            Simpan
          </Button>
        </DialogActions>
      </Dialog>

      {/* DIALOG FOR DELETE CampussDetail */}
      <Dialog
        open={alertDelete}
        onClose={() => {
          setAlertDelete(false);
        }}
        aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Peringatan!</DialogTitle>
        <DialogContent>Anda yakin ingin menghapus data ini?</DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              setAlertDelete(false);
            }}
            color="primary">
            Tutup
          </Button>
          <Button
            onClick={() => {
              DeleteCampussDetail(selectedIDCampussDetail);
            }}
            color="primary"
            variant="contained">
            Hapus
          </Button>
        </DialogActions>
      </Dialog>

      {/* ALERT FOR SUCCESS/ERROR */}
      <Snackbar
        autoHideDuration={3000}
        ContentProps={{
          'aria-describedby': 'message-id'
        }}
        message={<span id="message-id">{messageSnackbar}</span>}
        onClose={() => setAlert(false)}
        open={alertt}
        TransitionComponent={Slide}
      />
    </div>
  );
};

export default Campuss;

import React, { useEffect, useState } from 'react';
import MyTable from '../../components/MyTablePagination';
import axios from '../../util/Service';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
  Grid,
  Card,
  Drawer,
  Divider,
  Button,
  IconButton,
  TextField,
  Snackbar,
  Slide,
  FormControlLabel,
  Switch
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const drawerWidth = '35%';
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  },
  content: {
    flexShrink: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    zIndex: '999'
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start'
  },
  buttonAdd: {
    margin: theme.spacing(3)
  }
}));

const fields = ['Title', 'Content'];
const fieldData = ['title', 'content'];

const CMS = () => {
  const classes = useStyles();
  const theme = useTheme();

  // CMS
  const [data, setData] = useState([]);
  const [form, setForm] = useState({
    guid_cms: null,
    title: "",
    content: ""
  });

  const [open, setOpen] = useState(false);
  const [page, setPage] = useState(1);
  const [pageCount, setPageCount] = useState(1);
  const [tableLoading, setTableLoading] = useState(false);

  // snackbar
  const [alertt, setAlert] = useState(false);
  const [messageSnackbar, setMessageSnackbar] = useState(null);

  useEffect(() => {
    getCMSes();
  }, [page]);

  // API
  const getCMSes = () => {
    axios({
      method: 'GET',
      url: `v1/cms?page=${page}&limit=10`,
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => {
        if (response.status === 200 && response.data.data.length != 0) {
          setPageCount(response.data.pagination);
          setData(response.data.data);
        }
      })
      .catch(error => {
        alert(error);
      });
  };

  const AddCMS = () => {
    let dataCMS = {
      title: form.title,
      content: form.content
    };

    axios({
      method: 'POST',
      url: `v1/cms`,
      headers: { 'Content-Type': 'application/json' },
      data: dataCMS
    })
      .then((response) => {
        handleShowSnackbar(true, "Berhasil menambahkan CMS");
        handleDrawerClose();
      })
      .catch(error => {
        alert(error);
      });
  };

  const UpdateCMS = () => {
    let dataCMS = {
      guid_cms: form.guid_cms,
      title: form.title,
      content: form.content
    };

    axios({
      method: 'PUT',
      url: `v1/cms`,
      data: dataCMS,
      headers: { 'Content-Type': 'application/json' }
    })
      .then(() => {
        setPage(1);
        handleDrawerClose();
        handleShowSnackbar(true, "Berhasil update data");
        getCMSes();
      })
      .catch(error => {
        alert(error);
      });
  };

  const deleteCMS = id => {
    axios({
      method: 'DELETE',
      url: `v1/cms`,
      headers: { 'Content-Type': 'application/json' },
      params: { guid_cms: id }
    })
      .then(() => {
        setPage(1);
        getCMSes();
        handleShowSnackbar(true, "Berhasil hapus data");
      })
      .catch(error => {
        alert(error);
      });
  };
  //

  // handle
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerOpenForEdit = guid_cms => {
    axios({
      method: 'GET',
      url: `v1/cms?guid_cms=` + guid_cms,
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => {
        const selectCMS = response.data.data[0];
        
        setForm(selectCMS);

        handleDrawerOpen();
      })
      .catch(error => {
        alert(error);
      });
  };

  const handleDrawerClose = () => {
    setOpen(false);
    setForm({
      guid_cms: null,
      title: "",
      content: ""
    });
  };

  const handleChangeForm = name => e => {
    
      setForm({
        ...form,
        [name]: e.target.value
      });
  };
  //

  const handleShowSnackbar = (status, message) => {
    setMessageSnackbar(message);
    setAlert(status);
  };

  const handleOnNextPage = () => setPage(page + 1);
  const handleOnBackPage = () => setPage(page - 1);

  return (
    <div className={classes.root}>
      <Grid container spacing={4}>
        <Grid item md={12} xs={12}>
          <Card className={classes.root} variant="outlined">
            <h4>CMS</h4>
            <hr style={{ marginBottom: '30px' }} />

            <MyTable
              id="guid_cms"
              data={data}
              fields={fields}
              fieldData={fieldData}
              onAdd={handleDrawerOpen}
              onDelete={deleteCMS}
              onEdit={handleDrawerOpenForEdit}
              onUpdate={() => {}}
              titleAdd="Tambah CMS"
              page={page}
              pageCount={pageCount}
              isLoading={tableLoading}
              onNextPageClicked={handleOnNextPage}
              onBackPageClicked={handleOnBackPage}
            />
          </Card>
        </Grid>
      </Grid>

      <Drawer
        className={classes.drawer}
        anchor="right"
        open={open}
        classes={{ paper: classes.drawerPaper }}>
        <div className={classes.drawerHeader}>
          <IconButton
            onClick={() => {
              handleDrawerClose();
            }}>
            {theme.direction === 'rtl' ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
          {form.guid_cms === null ? 'Simpan' : 'Ubah'} Informasi CMS
          <Button
            variant="contained"
            color="primary"
            style={{ position: 'absolute', right: '20px', top: '-10px' }}
            className={classes.buttonAdd}
            onClick={
              form.guid_cms === null ? () => AddCMS() : () => UpdateCMS()
            }>
            {form.guid_cms === null ? 'Simpan' : 'Ubah'}
          </Button>
        </div>
        <Divider />
        <Grid container style={{ padding: theme.spacing(3) }}>
          <Grid item xs={6}>
            <h5>Informasi CMS</h5>
            <div className="d-flex justify-content-start">
              <FormControlLabel
                control={
                  <Switch
                    checked={form.is_active}
                    onChange={handleChangeForm('is_active')}
                    name="Status_Active"
                    color="primary"
                  />
                }
                label={form.is_active ? "Aktif" : "Tidak Aktif"}
              />
            </div>
            <Grid item xs={10}>
              <TextField
                id="title"
                label="Judul"
                value={form.title}
                onChange={handleChangeForm('title')}
                margin="normal"
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={10}>
              <TextField
                id="Content"
                label="Konten HTML"
                value={form.Content}
                onChange={handleChangeForm('Content')}
                margin="normal"
                fullWidth
                variant="outlined"
              />
            </Grid>
          </Grid>
        </Grid>
        <Divider />
      </Drawer>

      {/* ALERT FOR SUCCESS/ERROR */}
      <Snackbar
        autoHideDuration={3000}
        ContentProps={{
          'aria-describedby': 'message-id'
        }}
        message={<span id="message-id">{messageSnackbar}</span>}
        onClose={() => setAlert(false)}
        open={alertt}
        TransitionComponent={Slide}
      />
    </div>
  );
};

export default CMS;

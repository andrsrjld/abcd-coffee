import React, { useState, useEffect } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
// import { makeStyles } from '@material-ui/styles';
import {
  Grid,
  Button,
  TextField,
  Typography,
  Snackbar,
  Slide,
} from '@material-ui/core';
import { API } from '../../constants';
import logo_dark from '../../assets/logo/abc-white.png';
import bg_login from '../../assets/logo/login_bg_all.png';
import { makeStyles } from "@material-ui/core/styles";

import fetch from 'isomorphic-unfetch';

// import { authenticationService } from '../../services'

const schema = {
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 64
    }
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.default,
    height: '100%'
  },
  grid: {
    height: '100%'
  },
  quoteContainer: {
    [theme.breakpoints.down('md')]: {
      display: 'none'
    }
  },
  quote: {
    backgroundColor: theme.palette.neutral,
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundImage: 'url(/images/auth.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  quoteInner: {
    textAlign: 'center',
    flexBasis: '600px',
  },
  quoteText: {
    color: theme.palette.white,
    fontWeight: 300,
  },
  name: {
    marginTop: theme.spacing(3),
    color: theme.palette.white,
  },
  bio: {
    color: theme.palette.white,
  },
  contentContainer: {},
  content: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  contentHeader: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: theme.spacing(5),
    paddingBototm: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  logoImage: {
    marginLeft: theme.spacing(4),
  },
  contentBody: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
    }
  },
  contentWrap: {
    display:'flex',
    flexBasis: 680,
    boxShadow: '5px 10px 18px #c1c1c1',
    overflow: 'hidden',
    borderRadius: '10px',
  },
  form: {
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
    },
    backgroundColor: '#fff',
    padding: '30px',
    width:'60%'
  },
  logoWrap: {
    backgroundColor: '#000',
    width:'40%'
  },
  title: {
    marginTop: theme.spacing(3)
  },
  socialButtons: {
    marginTop: theme.spacing(3)
  },
  socialIcon: {
    marginRight: theme.spacing(1)
  },
  sugestion: {
    marginTop: theme.spacing(2)
  },
  textField: {
    marginTop: theme.spacing(2)
  },
  signInButton: {
    margin: theme.spacing(2, 0)
  }
}));

const SignIn = props => {
  const { history } = props;

  const classes = useStyles();

  const currentUser = JSON.parse(localStorage.getItem('currentUser'));

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  // SHOWING SNACKBAR ERROR
  const [alertt, setAlert] = useState(false);
  const [messageSnackbar, setMessageSnackbar] = useState('');

  useEffect(() => {

    if (currentUser) {
      history.push('/users');
      console.log('aa')
    }
    // const errors = validate(formState.values, schema);

    // setFormState(formState => ({
    //   ...formState,
    //   isValid: errors ? false : true,
    //   errors: errors || {}
    // }));
    
  }, [formState.values]);

  const handleBack = () => {
    history.goBack();
  };

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const doLogin = async (e) => {

    const userData = {
      email: formState.values.email,
      password: formState.values.password
    }
    
    e.preventDefault();
    try {
      const api = await fetch(`${API}/auth/login`, {
        method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(userData),
      });
      const res = await api.json();
      
      // if api error
      if (!api.ok) throw new Error('email atau password salah');
      // console.clear();

      // if api success
      console.log(res);
      if (res.message === '00') {

        // const apiProfile = await fetch(`${API}/api/profile`, {
        //   method: 'GET',
        //   headers: { 'Authorization': `Bearer ${res.data.token}` }
        // });
        // const response = await apiProfile.json();

        // if (response.message === '00') {
          localStorage.setItem('token', JSON.stringify(res.data));
          localStorage.setItem('currentUser', JSON.stringify(res.data));
          
          window.location.href = '/users';
        // }
      }
    }
    catch (err) {
      handleShowSnackbar(true, String(err));
    }
  }

  const handleShowSnackbar = (status, err) => {
    setMessageSnackbar(err);
    setAlert(status);
  }

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;
    

  const onEnterPress = (e) => {
    if(e.keyCode == 13 && e.shiftKey == false) {
      e.preventDefault();
      // const {
      //   email,
      //   password
      // } = this.state;
      // this.props.userSignIn({email,password})
      doLogin(e);
    }
  }

  return (
    <div className="w-100"
       style={{ background: `url(${require("assets/logo/login_bg_all.png")})`, height:  '100vh', marginTop:'-64px' }}>
      <Grid
        className={classes.grid}
        container
      >
        <Grid
          className={classes.content}
          item
          lg={12}
          xs={12}
        >
          <div className={classes.content} style={{ marginLeft: '23%' }}>
            <div className={classes.contentBody}>
              <div className={classes.contentWrap}>
                <form
                  className={classes.form}
                  onSubmit={doLogin}
                >
                  <Typography
                    className={classes.title}
                    variant="h3"
                  >
                    Login
                  </Typography>
                  <TextField
                    className={classes.textField}
                    fullWidth
                    helperText={
                      hasError('email') ? formState.errors.email[0] : null
                    }
                    label="email"
                    name="email"
                    onChange={handleChange}
                    type="text"
                    value={formState.values.email || ''}
                    // variant="outlined"
                  />
                  <TextField
                    className={classes.textField}
                    error={hasError('password')}
                    fullWidth
                    helperText={
                      hasError('password') ? formState.errors.password[0] : null
                    }
                    label="Password"
                    name="password"
                    onChange={handleChange}
                    type="password"
                    value={formState.values.password || ''}
                    // variant="outlined"
                  />
                  <Button
                    className={classes.signInButton}
                    color="primary"
                    size="large"
                    type="submit"
                    variant="contained"
                  >
                    Sign in
                  </Button>
                </form>
                <div className={classes.logoWrap}>
                  <img src={logo_dark} style={{ width: '200px', margin: '115px auto', display: 'block' }} />
                </div>
              </div>
            </div>
          </div>
        </Grid>
      </Grid>

      {/* ALERT FOR ERROR */}
      <Snackbar
        open={alertt}
        onClose={() => setAlert(false)}
        TransitionComponent={Slide}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        autoHideDuration={3000}
        message={<span id="message-id">{messageSnackbar}</span>}
      />
    </div>
  );
};

SignIn.propTypes = {
  history: PropTypes.object
};

export default withRouter(SignIn);

import React, { useEffect, useState } from 'react';
import {
  Grid, Card, Drawer, Divider, Button, IconButton, TextField, Typography, Slide, Snackbar,
  OutlinedInput, InputAdornment, FormControl, InputLabel
} from '@material-ui/core';
import MySelect from '../../components/Select';
import { useTheme } from '@material-ui/core/styles';
import { API } from '../../constants';
import MyTable from "../../components/MyTable";
import useStyles from './styles';

// Icons
import {
  ChevronLeft as ChevronLeftIcon,
  ChevronRight as ChevronRightIcon,
  Visibility, VisibilityOff
} from '@material-ui/icons';

const User = () => {
  const classes = useStyles();
  const theme = useTheme();

  // table data
  const fields = ['Email', 'Nama', 'No Telp'];
  const fieldData = ['email', 'fullname', 'phone_number'];

  // state region
  const [authData, setAuthData] = useState(null);
  const [userToken, setUserToken] = useState(null);
  const [userData, setUserData] = useState([]);
  const [roles, setRoles] = useState([]);
  const [roleData, setRoleData] = useState([]);
  const [open, setOpen] = useState(false);
  const [page, setPage] = useState(1);
  const [showSnackbar, setShowSnackbar] = useState(false);
  const [messageSnackbar, setMessageSnackbar] = useState(null);
  const [showPassword, setShowPassword] = useState(false);
  const [form, setForm] = useState({
    guid_user: null,
    username: '',
    email: '',
    phone_number: '',
    birth_place: '',
    dob: '',
    bio: '',
    link: '',
    gender: '',
    fullname: '',
    password: '',
    address: ''
  });

  useEffect(() => {
    const tempToken = JSON.parse(localStorage.getItem('token'));
    const currUser = JSON.parse(localStorage.getItem('currentUser'));

    if (tempToken) {
      setAuthData(currUser);
      setUserToken(tempToken.token);
      
      getAllUser(tempToken.token);
      // fetchRoles(tempToken.token);
    } else {
      handleShowSnackbar(true, "Sesi anda sudah berakhir, harap login kembali!");
      localStorage.clear();
      window.location.href = "/sign-in";
    }
  }, []);

  // const conditionByRole = x => {
  //   const { role } = authData;
  //   switch (role) {
  //     case 0:
  //       return x.role !== 0;
  //     case 1:
  //       return x.role !== 0 && x.role !== 1;
  //     case 2:
  //       setIsStoreDisabled(true);
  //       setCanFetchStore(false);
  //       return x.role !== 0 && x.role !== 1 && x.role !== 2;
  //     default:
  //       return x.role !== 0 && x.role !== 1 && x.role !== 2;
  //   }
  // }

  const getAllUser = async (userToken) => {

    const api = await fetch(`${API}/v1/user`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${userToken}`
      },
    });
    if (!api.ok) {
      if (api.status === 401) {
        handleShowSnackbar(true, "Sesi anda sudah berakhir, harap login kembali!");
        localStorage.removeItem('token');
        localStorage.removeItem('currentUser');
        window.location.href = '/sign-in';
      } else {
        handleShowSnackbar(true, api.status);
      }
      return;
    }
    const res = await api.json();
    if (res && res.data && res.message === '00') {
      // populate user list
      const { data } = res;
      // let tempUser = [];
      // data.map(x => {
      //   // hide superadmin :p
      //   if (conditionByRole(x)) {
      //     x.role_id = x.role;
      //     x.role = roles.find(v => v.id === x.role).role;
      //     tempUser.push(x);
      //   }
      // });
      setUserData(data);
    } else {
      handleShowSnackbar(true, res.message ? res.message : "Unknown error. Please contact developer!");
    }
  }

  const fetchRoles = async (userToken) => {
    const api = await fetch(`${API}/api/roles`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${userToken}`
      }
    });
    if (!api.ok) {
      // if (api.status === 401) {
      //   handleShowSnackbar(true, "Sesi anda sudah berakhir, harap login kembali!");
      //   localStorage.clear();
      //   window.location.href = "/sign-in";
      //   return;
      // }
      handleShowSnackbar(true, api.statusText);
      return;
    }
    const res = await api.json();
    if (res && res.message && res.message === '00') {
      setRoles(res.data);
      // populate roles
      // // remove superadmin
      // let tempRoles = [];
      // res.data.map(v => {
      //   if (v.id !== 0) {
      //     v.label = v.role;
      //     v.value = v.id;
      //     tempRoles.push(v);
      //   }
      // })
      // setRoleData(tempRoles);
      setRoleData(res.data.map(v => ({...v, label: v.role_name, value : v.role_id}) ));
    } else {
      handleShowSnackbar(true, res.message ? res.message : "Unknown error. Please contact developer!");
    }
  }

  // useEffect(() => {
  //   if (token) {
  //     fetchRoles(token);
  //   }
  //   // if (token) {
  //   //   getAllUser(token);
  //   // }
  // }, [token]);

  // useEffect(() => {
  //   if (token) {
  //     getAllUser(token);
  //   }
  // }, [roles]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const [selectedUser, setSelectedUser] = useState(null);

  const handleDrawerOpenForEdit = (id) => {
    const selectedUser = userData.find(x => x.id === id);
    setSelectedUser(selectedUser);

    const role = roleData.find(x => x.role_id === selectedUser.role_id);

    setForm({
      id: selectedUser.id,
      email: selectedUser.email,
      name: selectedUser.name,
      role: role,
      phone_number: selectedUser.phone_number,
    });
    setOpen(true);
  }

  const handleDrawerClose = () => {
    setOpen(false);
    setForm({
      guid_user: null,
      // email: '',
      // password: '',
      // name: '',
      // role: null,
      // phone_number: '',
      username: '',
      email: '',
      phone_number: '',
      birth_place: '',
      dob: '',
      bio: '',
      link: '',
      gender: '',
      fullname: '',
      password: '',
      address: ''
    });
  };

  const handleChangeSelect = type => value => {
    setForm({
      ...form, [type]: value
    });
  }

  const handleChangeForm = type => e => {
    setForm({
      ...form, [type]: e.target.value
    })
  }

  const toggleShowHidePassword = () => setShowPassword(!showPassword);

  const handleMouseDownPassword = e => e.preventDefault();

  const validateForm = () => form.email && form.email.length > 0 &&
    form.name && form.name.length > 0 &&
    form.password && form.password.length > 0 &&
    form.phone_number && form.phone_number.length > 0 &&
    form.role;

  const validateFormEdit = () => form.email && form.email.length > 0 &&
    form.name && form.name.length > 0 && 
    form.phone_number && form.phone_number.length > 0 &&
    form.role;

  const deleteUser = async id => {
    const api = await fetch(`${API}/v1/user?guid_user=${id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${userToken}`
      }
    });
    if (!api.ok) {
      handleShowSnackbar(true, api.statusText);
      return;
    }
    const res = await api.json();
    if (res.message === '00') {
      handleShowSnackbar(true, 'Berhasil hapus user');
      getAllUser(userToken);
    } else {
      handleShowSnackbar(true, res.message);
    }
  }

  const createOrEditUser = () => {
    if (form.id) {
      // create new user
      if (!validateFormEdit()) {
        handleShowSnackbar(true, "Mohon isi semua field!");
        return;
      }
      console.log()

      updateUser();
    } else {
      // create new user
      if (!validateForm()) {
        handleShowSnackbar(true, "Mohon isi semua field!");
        return;
      }

      createUser();
    }
  }

  const createUser = async () => {
    const body = {
      // name: form.name,
      // phone_number: form.phone_number,
      // email: form.email,
      // password: form.password,
      // role_id: form.role.role_id,
      username: form.username,
      email: form.email,
      phone_number: form.phone_number,
      birth_place: form.birth_place,
      dob: form.dob,
      bio: form.bio,
      link: form.link,
      gender: form.gender,
      fullname: form.fullname,
      password: form.password,
      address: form.address
    }

    const api = await fetch(`${API}/api/register`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Authorization': `Bearer ${userToken}`,
        'Content-Type': 'application/json'
      }
    });

    if (!api.ok) {
      // if (api.status === 401) {
      //   handleShowSnackbar(true, "Sesi anda sudah berakhir, harap login kembali!");
      //   localStorage.clear();
      //   window.location.href = "/sign-in";
      //   return;
      // }
      const err = await api.json();
      handleShowSnackbar(true, err.message);
      return;
    }

    const res = await api.json();
    if (res.message === '00') {
      // 00 create user
      handleDrawerClose();
      getAllUser(userToken);
    }
  }

  const updateUser = async () => {
    const body = {
      id: form.id,
      email: form.email,
      name: form.name,
      phone_number: form.phone_number,
      role_id: form.role.role_id,
    }

    console.log(body);

    const api = await fetch(`${API}/v1/user`, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: {
        'Authorization': `Bearer ${userToken}`,
        'Content-Type': 'application/json'
      }
    });

    if (!api.ok) {
      // if (api.status === 401) {
      //   handleShowSnackbar(true, "Sesi anda sudah berakhir, harap login kembali!");
      //   localStorage.clear();
      //   window.location.href = "/sign-in";
      //   return;
      // }
      const err = await api.json();
      handleShowSnackbar(true, err.message);
      return;
    }

    const res = await api.json();
    if (res.message === '00') {
      // 00 create user
      handleDrawerClose();
      getAllUser(userToken);
    }
  }

  const handleShowSnackbar = (status, message) => {
    setMessageSnackbar(message);
    setShowSnackbar(status);
  }

  const handleOnNextPage = () => setPage(page + 1);

  const handleOnBackPage = () => setPage(page - 1);

  return (
    <div className={classes.root}>
      <Grid container spacing={4}>
        <Grid item md={12} xs={12}>
          <Card className={classes.root} variant="outlined">
            <h4>User</h4>
            <hr style={{ marginBottom: '30px' }} />
            {/* {
              userData ? */}
                <MyTable
                  id="guid_user"
                  data={userData}
                  fields={fields}
                  fieldData={fieldData}
                  onAdd={handleDrawerOpen}
                  onDelete={deleteUser}
                  onEdit={handleDrawerOpenForEdit}
                  onUpdate={() => { }}
                  titleAdd="Tambah User"
                  page={page}
                  onNextPageClicked={handleOnNextPage}
                  onBackPageClicked={handleOnBackPage}
                /> 
            {/*      :
                 null
             } */}
          </Card>
        </Grid>
      </Grid>
      <Drawer
        className={classes.drawer}
        anchor="right"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={() => {
            handleDrawerClose()
          }}>
            {theme.direction === 'rtl' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
          <Typography variant="h5">{form.id == null ? "Simpan" : "Ubah"} Informasi User</Typography>
          <Button variant="contained"
            color="primary"
            className={classes.buttonAdd}
            onClick={createOrEditUser}
          >
            {form.id == null ? "Simpan" : "Ubah"}
          </Button>
        </div>
        <Divider />
        <Grid container style={{ padding: theme.spacing(3) }}>
          <Grid item xs={12}>
            <TextField
              id="email"
              label="Email"
              value={form.email}
              onChange={handleChangeForm('email')}
              margin="normal"
              className={form && form.id ? "w-100" : "w-50 pr-1"}
              variant="outlined"
              disabled={form.id != null}
            />
            {
              form && form.id ?
                null :
                <FormControl variant="outlined"
                  margin="normal"
                  className="w-50 pl-1"
                >
                  <InputLabel htmlFor="user-password">Password</InputLabel>
                  <OutlinedInput
                    id="user-password"
                    type={showPassword ? 'text' : 'password'}
                    value={form.password}
                    placeholder="Password"
                    onChange={handleChangeForm('password')}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={toggleShowHidePassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                    labelWidth={70}
                  />
                </FormControl>
            }
            <TextField
              id="name"
              label="Nama"
              value={form.name}
              onChange={handleChangeForm('name')}
              margin="normal"
              fullWidth
              className="mt-0"
              variant="outlined"
            />
            <TextField
              id="phone_number"
              label="Nomor Telepon"
              value={form.phone_number}
              onChange={handleChangeForm('phone_number')}
              margin="normal"
              fullWidth
              className="mt-0"
              variant="outlined"
            />
            <div className="MuiFormControl-marginNormal mt-0">
              <MySelect
                name="Role"
                placeholder="Pilih Role"
                options={roleData}
                value={form.role}
                onChange={handleChangeSelect('role')}
              />
            </div>
          </Grid>
        </Grid>
      </Drawer>
      {/* Snackbar Component */}
      <Snackbar
        open={showSnackbar}
        onClose={() => setShowSnackbar(false)}
        TransitionComponent={Slide}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        autoHideDuration={3000}
        message={<span id="message-id">{messageSnackbar}</span>}
      />
    </div >
  )
}

export default User;
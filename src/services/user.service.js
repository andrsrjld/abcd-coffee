import { BehaviorSubject } from 'rxjs'
import axios from 'axios'
import { API } from '../constants'
import { Redirect } from 'react-router-dom';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));
// const currentUserSubject = new BehaviorSubject(localStorage.getItem('currentUser'));

export const authenticationService = {
    login,
    logout,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() { return currentUserSubject.value }
};

function login(email, password) {

    return axios({
        // method : 'POST',
        // url: `${API}/user`,
        // headers: {
        //     apikey: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBOYW1lIjoieW9naWUiLCJleHAiOjE4ODQyMjUyODQsImlzcyI6IldFU1MifQ.zqFTSm4Adm1NFQOg9_MKlA9ggknh7kYwTkcYxQmpfto'
        // },
        // params: {
        //   Action: 'Login'
        // },
        // data: {
        //   username: email,
        //   password: password
        // }
        method: 'GET',
        url: `${API}/admin/login`,
        auth: {
            username: email,
            password: password
        }
    }).then(response => {
        console.log(response)
        // if(response.data.Code === 200 && response.data.Data != null){
        if (response.status === 200) {
            console.log('berhasil login')
            console.log(response.data.data)
            // localStorage.setItem('token', response.data.Data[0].token)
            // localStorage.setItem('currentUser', JSON.stringify(response.data.Data[0]))
            // axios.defaults.headers.common['JWTClaim'] = "Bearer "+response.data.Data[0].token

            // currentUserSubject.next(response.data.Data[0]);

            const data = ({
                token: response.data.data,
                username: email
            })

            console.log(data)

            localStorage.setItem('token', response.data.data)
            localStorage.setItem('currentUser', JSON.stringify(data))
            currentUserSubject.next(data);
            return response;
        } else {
            // alert('email atau password salah')
            authenticationService.logout();
            // window.location.reload(true);
            throw 'email atau password salah';
        }
    }).catch(error => {
        // alert(error)
        alert('email atau password tidak cocok')
    })
    // }

    // const requestOptions = {
    //     method: 'POST',
    //     headers: { 'Content-Type': 'application/json' },
    //     body: JSON.stringify({ username, password })
    // };

    // return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
    //     .then(handleResponse)
    //     .then(user => {
    //         // store user details and jwt token in local storage to keep user logged in between page refreshes
    //         localStorage.setItem('currentUser', JSON.stringify(user));
    //         currentUserSubject.next(user);

    //         return user;
    //     });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    currentUserSubject.next(null);
}
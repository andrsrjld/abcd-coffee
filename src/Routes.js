import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { RoutePublic } from './components';

import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {
  SignIn as SignInView,
  NotFound as NotFoundView,
  Campuss as CampussView,
  CMS as CMSView,
  Role as RoleView,
  User as UserView,
  Profile as ProfileView,
} from './views';

const Routes = () => {
  return (
    <Switch>
      <RoutePublic
        component={SignInView}
        exact
        layout={MinimalLayout}
        path="/sign-in"
      />
      <RouteWithLayout
        component={UserView}
        exact
        layout={MainLayout}
        path="/users"
      />
      <RouteWithLayout
        component={CampussView}
        exact
        layout={MainLayout}
        path="/campuss"
      />
      <RouteWithLayout
        component={CMSView}
        exact
        layout={MainLayout}
        path="/cms"
      />
      <RouteWithLayout
        component={RoleView}
        exact
        layout={MainLayout}
        path="/role"
      />
      <RouteWithLayout
        component={ProfileView}
        exact
        layout={MainLayout}
        path="/profile"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
      <Redirect to="/sign-in" />
    </Switch>
  );
};

export default Routes;

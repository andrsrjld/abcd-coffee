import axios from './Service';
import moment from 'moment';

export default class ServiceApi {

  // Banner
  getBanners = () => {
    return axios({
        method: 'GET',
        url: `${API}/banners`,
        headers: {
          "Content-Type": "application/json"
        },
    })
  }
  
}

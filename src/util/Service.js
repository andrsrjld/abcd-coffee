import axios from 'axios';

const API_NEW = process.env.REACT_APP_BASE_URL;

const currUser = JSON.parse(localStorage.getItem('currentUser'));
const { token } = currUser ? currUser : {};

let service = axios.create({
  baseURL: API_NEW,
  headers: {
    'Content-Type': 'application/json',
  }
});

service.interceptors.request.use(function (config) {
  config.headers.Authorization =  'Bearer ' + token;

  return config;
});

service.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    // Return any error which is not due to authentication back to the calling service

    if (error.response.status !== 401) {
      return new Promise((resolve, reject) => {
        reject(error);
      });
    } else {
      if (localStorage.getItem('token')) {
        alert('Sesi anda sudah berakhir, harap login kembali!');
        localStorage.removeItem('token');
        localStorage.removeItem('currentUser');
        window.location.href = '/sign-in';
      }
    }
  }
);

export default service;

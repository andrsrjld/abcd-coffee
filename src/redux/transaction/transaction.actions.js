import transactionTypes from './transaction.types'

export const setTransaction = data => ({
  type: transactionTypes.SET_SELECTED_TRANSACTION,
  payload: data,
})
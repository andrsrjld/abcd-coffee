import transactionTypes from './transaction.types'

const INITIAL_STATE = {
  transaction: {}
}

const transactionReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case transactionTypes.SET_SELECTED_TRANSACTION:
      return {
        ...state, transaction: action.payload
      }
    default:
      return { ...state }
  }
}

export default transactionReducer
export const transactionTypes = {
  SET_SELECTED_TRANSACTION: 'SET_SELECTED_TRANSACTION'
}

export default transactionTypes
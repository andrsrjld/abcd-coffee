import authTypes from './auth.types'

export const setToken = data => ({
  type: authTypes.SET_TOKEN,
  payload: data,
})
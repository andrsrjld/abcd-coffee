import { combineReducers } from 'redux'
import UploadFile from './uploadFile/uploadFile.reducer'
import Auth from './auth/auth.reducer'
import Transaction from './transaction/transaction.reducer'

const rootReducer = combineReducers({
  UploadFile, Auth, Transaction
})

export default rootReducer